#include <jni.h>
#include <string>




int generateACrash6()
{
    *(int*)0 = 0;
    return 0;
}
int generateACrash5()
{
    return generateACrash6();
}
int generateACrash4()
{
    return generateACrash5();
}
int generateACrash3()
{
    return generateACrash4();
}
int generateACrash2()
{
    return generateACrash3();
}
int generateACrash()
{
    return generateACrash2();
}





extern "C" JNIEXPORT jstring JNICALL
Java_com_pixowl_pacmantemp_MainActivity_CRASHJNI(
        JNIEnv *env,
        jobject /* this */) {
    int a = generateACrash();
    return env->NewStringUTF("crash generated");
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_pixowl_pacmantemp_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
