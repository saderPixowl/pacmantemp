#!/bin/bash

#AAB_PATH=./build/outputs/bundle/devGoogleplayRelease/app.aab

AAB_PATH=./build/outputs/bundle/Release/app.aab
APK_PATH=./build/outputs/apk/release/


OUT_PATH=./build/outputs/apk/extracted_from_aab
OUT_APK=my_app.apks
BUNDLE_JAR=bundletool-all-0.9.0.jar

STORE_FILE=../Pixowl_Distribution.keystore
STORE_PASS=pass:P3anu75!
KEY_PASS=pass:P3anu75!

VARIANT=Release

APK_NAME=app-release.apk
#



../gradlew bundle$VARIANT --warning-mode all
#../gradlew bundleRelease


#--connected-device
#--mode=universal




../gradlew crashlyticsUploadSymbolsRelease
#adb install $APK_PATH/app-release.apk


java -jar $BUNDLE_JAR build-apks --bundle=$AAB_PATH --output=$OUT_PATH/$OUT_APK --overwrite  --ks=$STORE_FILE --ks-pass=$STORE_PASS --ks-key-alias="pixowl, inc." --key-pass=$KEY_PASS
java -jar $BUNDLE_JAR install-apks --apks=$OUT_PATH/$OUT_APK
